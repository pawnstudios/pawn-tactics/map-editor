/*
    This file is part of PTME.

    PTME is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    PTME is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with PTME. If not, see <https://www.gnu.org/licenses/>.
 */

package ptme;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.geom.AffineTransform;

import tiled.core.Map;
import tiled.core.TileSet;
import tiled.io.MapWriter;
import tiled.io.PluginLogger;
import tiled.core.Tile;
import tiled.core.TileLayer;

public class PTMapWriter implements MapWriter {

	private PluginLogger logger;

	public void writeMap(Map map, String filename) throws Exception {
		writeMap(map, new FileOutputStream(filename));
	}

	public void writeMap(Map map, OutputStream out) throws Exception {
		if (map.getTilesets().size() == 0) {
			logger.error("You must load a tileset first.");
			return;
		}
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));
		TileLayer tiles = (TileLayer) map.getLayer(0);

		writer.write(map.getWidth() + "&" + map.getHeight() + "&"
				+ map.getTilesets().get(0).getName() + "&");

		String spawns = "";
		Tile temp = tiles.getDefaultTile();
		temp.getProperties().put("Level",0);
		tiles.setDefaultTile(temp);		

		for (int i = 0; i < tiles.getHeight(); i++) {
			for (int o = 0; o < tiles.getWidth(); o++) {
				Tile t = tiles.getTileAt(o, i);
				if (t != null && (Integer) t.getProperties().get("Spawn") != -1) {
					spawns += (Integer) t.getProperties().get("Spawn") + "$"
							+ o + "," + i + "^";
				}
			}
		}
		if (spawns.length() > 0) {
			spawns = spawns.substring(0, spawns.length() - 1);
		}
		writer.write(spawns + "&");

		String tileData = "";

		for (int i = 0; i < tiles.getHeight(); i++) {
			for (int o = 0; o < tiles.getWidth(); o++) {
				Tile t = tiles.getTileAt(o, i);
				if (!t.equals(temp)) {
					AffineTransform transform = t.getTransform();
					double[] matrix = new double[4];
					transform.getMatrix(matrix);
					tileData += o + "," + i + "$" + t.getImageId() + ","
							+ (int) (matrix[0] + 1) + ""
							+ (int) (matrix[1] + 1) + ""
							+ (int) (matrix[2] + 1) + ""
							+ (int) (matrix[3] + 1) + ","
							+ (Integer) t.getProperties().get("Level") + ","
							+ (Integer) t.getProperties().get("Special") + "^";
				}
			}
		}
		if (tileData.length() > 0) {
			tileData = tileData.substring(0, tileData.length() - 1);
		}
		writer.write(tileData + "&");

		writer.flush();
		writer.close();
	}

	public void writeTileset(TileSet set, String filename) throws Exception {
		writeTileset(set, new FileOutputStream(filename));
	}

	public void writeTileset(TileSet set, OutputStream out) throws Exception {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out));

		// Write tileset name
		writer.write(set.getName() + "&");

		for (Enumeration i = set.getImageIds(); i.hasMoreElements();) {
			Tile t = set.getTile(Integer.parseInt((String) i.nextElement()));

			writer.write(t.getId() + "$");

			for (Iterator o = t.getProperties().entrySet().iterator(); o
					.hasNext();) {
				Entry item = (Entry) o.next();
				writer.write(item.getKey() + ":" + item.getValue());
				writer.write(',');
			}

			writer.write('$');
			writer.flush();

			ImageIO.write((BufferedImage) t.getImage(), "png", out);
		}

		writer.flush();
		writer.close();
	}

	public String getDescription() {
		return "Writes Pawn Tactics map files.";
	}

	public String getFilter() throws Exception {
		return "ptm";
	}

	public String getName() {
		return "Pawn Tactics Map";
	}

	public String getPluginPackage() {
		return "Pawn Tactics Writer Plugin";
	}

	public void setLogger(PluginLogger logger) {
		this.logger = logger;
	}

	public boolean accept(File pathname) {
		try {
			String path = pathname.getCanonicalPath();
			if (path.endsWith(".ptm")) {
				return true;
			}
		} catch (IOException e) {
		}
		return false;
	}

}

/*
    This file is part of PTME.

    PTME is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    PTME is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with PTME. If not, see <https://www.gnu.org/licenses/>.
 */

package ptme;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

import tiled.core.Map;
import tiled.core.TileSet;
import tiled.io.PluginLogger;
import tiled.io.PluggableMapIO;
import tiled.core.Tile;

public class PTTilesetFilter implements PluggableMapIO, FileFilter {
	
	private PluginLogger logger;

	public String getDescription() {
		return "Writes Pawn Tactics tileset files.";
	}

	public String getFilter() throws Exception {
		return "ptx";
	}

	public String getName() {
		return "Pawn Tactics Tileset";
	}

	public String getPluginPackage() {
		return "Pawn Tactics Writer Plugin";
	}

	public void setLogger(PluginLogger logger) {
		this.logger = logger;
	}

	public boolean accept(File pathname) {
		try {
			String path = pathname.getCanonicalPath();
			if (path.endsWith(".ptx")) {
				return true;
			}
		} catch (IOException e) {}
		return false;
	}

}

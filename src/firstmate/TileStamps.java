/*
    This file is part of PTME.

    PTME is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    PTME is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with PTME. If not, see <https://www.gnu.org/licenses/>.
 */

package firstmate;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Vector;

import tiled.core.Tile;
import tiled.core.TileSet;
import tiled.io.MapHelper;

public class TileStamps {
	private Boolean hasLoaded;
	private LinkedList<Stamp> stamps;
	public Vector<TileSet> sets;
	private Stamp lastStamp;
	
	private String tSet; 
	public TileStamps(String file, Vector<TileSet> sets) {
		stamps = new LinkedList<Stamp>();
		this.sets = sets;
		tSet = sets.get(0).getName();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = "";
			String tileset, tempTrigger;
			int trigger, width, height;

			while ((line = reader.readLine()) != null) {
				// Commented line prefixed with #
				if (!line.substring(0, 1).equals("#")) {
					// Syntax
					// tileid represents the tile that will trigger the 'set'
					// x,y coords are 0-based.
					// Neglected tiles will be the default
					// Ranges?
					// tileset&tileid$w,h&x,y$tileid,rot,spawn,special^x1,y1$tileid,rot,spawn,special
					String[] params = line.split("&");
					tileset = params[0].split("\\.")[0];
					tempTrigger = params[1];
					// 24$1,1
					TileSet set = null;
					if (sets.get(0).getName().equalsIgnoreCase(tileset)) {
						set = sets.get(0);
					}
					
					trigger = Integer.parseInt(tempTrigger.split("\\$")[0]);
					width = Integer.parseInt(tempTrigger.split("\\$")[1]
							.split("\\,")[0]);
					height = Integer.parseInt(tempTrigger.split("\\$")[1]
							.split("\\,")[1]);
					Stamp temp = null;
					if (set != null) {
						temp = new Stamp(set, tileset, trigger, width, height);
					}
					String[] stampLen = params[2].split("\\^");
					int tx, ty, tid, theight, special;
					for (int i = 0; i < stampLen.length; i++) {
						// x,y$tileid
						tx = Integer.parseInt(stampLen[i].split("\\$")[0]
								.split("\\,")[0]);
						ty = Integer.parseInt(stampLen[i].split("\\$")[0]
								.split("\\,")[1]);
						String[] props = stampLen[i].split("\\$")[1]
								.split("\\,");
						tid = Integer.parseInt(props[0]);
						double[] matrix = new double[4];
						matrix[0] = Integer.parseInt(props[1].substring(0, 1)) - 1;
						matrix[1] = Integer.parseInt(props[1].substring(1, 2)) - 1;
						matrix[2] = Integer.parseInt(props[1].substring(2, 3)) - 1;
						matrix[3] = Integer.parseInt(props[1].substring(3, 4)) - 1;
						theight = Integer.parseInt(props[2]);
						special = Integer.parseInt(props[3]);
						if (tx < width && ty < height && set != null) {
							temp.setId(tx, ty, tid, matrix, theight, special);
							// temp.setId(tx,ty,tid);
						}
					}
					if (set != null) {
						stamps.add(temp);
					}
				}
			}
			if (stamps.size() != 0) {
				hasLoaded = true;
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			hasLoaded = false;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getTileSet() {
		return tSet;
	}

	public Boolean hasLoaded() {
		return hasLoaded;
	}

	public Boolean hasTriggered(Tile t) {
		Boolean ret = false;
		for (Stamp s : stamps) {
			if (s.getTileSet().equalsIgnoreCase(t.getTileSet().toString())
					&& s.getTrigger() == t.getImageId()) {
				lastStamp = s;
				ret = true;
			}
		}
		return ret;
	}

	public Stamp getStamp() {
		return lastStamp;
	}
}

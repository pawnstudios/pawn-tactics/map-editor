/*
 *  Tiled Map Editor, (c) 2004-2006
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Adam Turk <aturk@biggeruniverse.com>
 *  Bjorn Lindeijer <b.lindeijer@xs4all.nl>
 */

package tiled.core;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.geom.AffineTransform;
import java.util.Iterator;
import java.util.Properties;
import java.util.Map.Entry;

import tiled.mapeditor.selection.SelectionLayer;

/**
 * The core class for our tiles.
 *
 * @version $Id$
 */
public class Tile
{
    private Image internalImage, scaledImage;
    private int id = -1;
    protected int tileImageId = -1;
    private int groundHeight;          // Height above/below "ground"
    private int tileOrientation;
    private double myZoom = 1.0;
    private Properties properties;
    private TileSet tileset;
    private AffineTransform transform = new AffineTransform();

    public Tile() {
        properties = new Properties();
    }

    public Tile(TileSet set) {
        this();
        setTileSet(set);
    }

    /**
     * Copy constructor
     *
     * @param t
     */
    public Tile(Tile t) {
    	this();
    	if (t != null) {
		    for (Iterator i = t.properties.entrySet().iterator(); i.hasNext(); ) {
		    	Entry item = (Entry)i.next();
		    	this.properties.put(item.getKey(), item.getValue());
		    }
		    transform = (AffineTransform)t.transform.clone();
	        tileImageId = t.tileImageId;
	        tileset = t.tileset;
	        if (tileset != null) {
	            scaledImage = getImage().getScaledInstance(
	                    -1, -1, Image.SCALE_DEFAULT);
	        }
    	}
    }

    /**
     * Sets the id of the tile as long as it is at least 0.
     *
     * @param i The id of the tile
     */
    public void setId(int i) {
        if (i >= 0) {
            id = i;
        }
    }

    /**
     * Changes the image of the tile as long as it is not null.
     *
     * @param i the new image of the tile
     */
    public void setImage(Image i) {
        if (tileset != null) {
            tileset.overlayImage(tileImageId, i);
        } else {
            internalImage = i;
        }
    }

    public void setImage(int id) {
        tileImageId = id;
    }

    /**
     * @deprecated
     * @param orientation
     */
    public void setImageOrientation(int orientation) {
        tileOrientation = orientation;
    }

    /**
     * Sets the parent tileset for a tile. If the tile is already
     * a member of a set, and this method is called with a different
     * set as argument, the tile image is transferred to the new set.
     *
     * @param set
     */
    public void setTileSet(TileSet set) {
        if (tileset != null && tileset != set) {
            setImage(set.addImage(getImage()));
        } else {
            if (internalImage != null) {
                setImage(set.addImage(internalImage));
                internalImage = null;
            }
        }
        tileset = set;
    }

    public void setProperties(Properties p) {
        properties = p;
    }

    public Properties getProperties() {
        return properties;
    }

    /**
     * Returns the tile id of this tile, relative to tileset.
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the global tile id by adding the tile id to the map-assigned.
     *
     * @return id
     */
    public int getGid() {
        if (tileset != null) {
            return id + tileset.getFirstGid();
        }
        return id;
    }

    /**
     * Returns the {@link tiled.core.TileSet} that this tile is part of.
     *
     * @return TileSet
     */
    public TileSet getTileSet() {
        return tileset;
    }

    /**
     * This drawing function handles drawing the tile image at the
     * specified zoom level. It will attempt to use a cached copy,
     * but will rescale if the requested zoom does not equal the
     * current cache zoom.
     *
     * @param g Graphics instance to draw to
     * @param x x-coord to draw tile at
     * @param y y-coord to draw tile at
     * @param zoom Zoom level to draw the tile
     * @param tileProperties Boolean toggling whether to display tile properties
     */
    public void drawRaw(Graphics g, int x, int y, double zoom) {
    	drawRaw(g,x,y,zoom,false);
    }
    public void drawRaw(Graphics g, int x, int y, double zoom, Boolean tileProperties) {
        Image img = getScaledImage(zoom);
        if (img != null) {
        	int width = img.getWidth(null);
        	int height = img.getWidth(null);
        	
        	Graphics2D g2d = (Graphics2D) g;

            int special = (Integer)this.getProperties().get("Special");
        	if (!tileProperties) {
	        	if (special < 39 && special > 6) {
		            g2d.setComposite(AlphaComposite.getInstance(
		                    AlphaComposite.SRC_ATOP, 0.5f));
		            g2d.setColor(Color.blue);
		            g2d.fillRect(x, y - height, width, height);
		            g2d.setColor(Color.blue);
        		}
        	}
            
        	AffineTransform orig = g2d.getTransform();
        	g2d.translate(x + (width / 2), y - (height / 2));
        	g2d.transform(transform);
            g2d.drawImage(img, -width / 2, -height / 2, null);
            g2d.setColor(Color.black);
            g2d.setTransform(orig);

            g2d.setComposite(AlphaComposite.getInstance(
                    AlphaComposite.SRC_ATOP, 1.0f));
            Color c = g2d.getColor();
            int spawn = (Integer)this.getProperties().get("Spawn");
            
            switch (spawn) {
            case 0:
            	g2d.setColor(Color.RED);
            	g2d.draw(new Rectangle(x + 1, y - (height - 1), width - 2, height - 2));
            	break;
            case 1:
            	g2d.setColor(Color.BLUE);
            	g2d.draw(new Rectangle(x + 1, y - (height - 1), width - 2, height - 2));
            	break;
            case 2:
            	g2d.setColor(Color.GREEN);
            	g2d.draw(new Rectangle(x + 1, y - (height - 1), width - 2, height - 2));
            	break;
            case 3:
            	g2d.setColor(Color.YELLOW);
            	g2d.draw(new Rectangle(x + 1, y - (height - 1), width - 2, height - 2));
            	break;
            case 4:
				g2d.setColor(Color.BLACK);
				g2d.draw(new Rectangle(x + 1, y - (height - 1), width - 2, height - 2));
				break;
            case 5:
            	g2d.setColor(Color.WHITE);
            	g2d.draw(new Rectangle(x + 1, y - (height - 1), width - 2, height - 2));
            	break;
            }
            
            if (tileProperties) {
	            if ((Integer)this.getProperties().get("Level") != 0) {
	            	g2d.setColor(Color.RED);
					g2d.drawLine(x, y - height, x + width, y);
	            }
	            if (special != 0) {
					g2d.setColor(Color.BLUE);
					g2d.drawLine(x, y, x + width, y - height);
				}
			
				g2d.setColor(Color.BLACK);
	            g2d.drawString(String.valueOf((Integer)this.getProperties().get("Level")), x + 3, y - height + 13);
	            g2d.drawString(String.valueOf(spawn), x + width - 10, y - height + 13);
	            g2d.drawString(String.valueOf(special), x + 3, y - 3);
	            g2d.setColor(c);
            } else {
            	if (special < 39 && special > 6) {
		            g2d.setColor(Color.red);
		            Font origFont = g2d.getFont();
		            g2d.setFont(new Font(origFont.getFontName(), Font.BOLD, origFont.getSize() + 4));
		            
		            int posX = x + (width /2) - 2;
		            int posY = y - (height /2) + 2;
		            
		            if (special == 7) {
		            	g2d.drawString("F", posX, posY);
		            } else {
		            	int zone = ((special - 8)/2) + 1;
		            	g2d.drawString(String.valueOf(zone), posX, posY);
		            }
		            g2d.setFont(origFont);
            	}
            }
            //TODO: fix this
            /*
			if ((Integer)this.getProperties().get("Special") < 35 && (Integer)this.getProperties().get("Special") > 6) {                
				g2d.setColor(new Color(100, 100, 255));
                g2d.draw3DRect(x,y,width,height,false);
                g2d.setComposite(AlphaComposite.getInstance(
                        AlphaComposite.SRC_ATOP, 0.6f));
                g2d.fillRect(x,y,width,height);
			}
			*/
            g2d.setColor(c);
        } else {
            // TODO: Allow drawing IDs when no image data exists as a
            // config option
        }
    }
    public void draw(Graphics g, int x, int y, double zoom) {
    	draw(g,x,y,zoom,false);
    }
    /**
     * Draws the tile at the given pixel coordinates in the given
     * graphics context, and at the given zoom level
     *
     * @param g
     * @param x
     * @param y
     * @param zoom
     * @param tileProperties 
     */
    public void draw(Graphics g, int x, int y, double zoom, Boolean tileProperties) {
        // Invoke raw draw function
        int gnd_h = (int)(groundHeight * zoom);
        drawRaw(g, x, y - gnd_h, zoom, tileProperties);
    }

    public int getWidth() {
        if (tileset != null) {
            Dimension d = tileset.getImageDimensions(tileImageId);
            return d.width;
        } else if (internalImage != null){
            return internalImage.getWidth(null);
        }
        return 0;
    }

    public int getHeight() {
        if (tileset != null) {
            Dimension d = tileset.getImageDimensions(tileImageId);
            return d.height;
        } else if (internalImage != null) {
            return internalImage.getHeight(null);
        }
        return 0;
    }

    public int getImageId() {
        return tileImageId;
    }

    /**
     * @deprecated
     * @return int
     */
    public int getImageOrientation() {
        return tileOrientation;
    }

    /**
     * Returns the tile image for this Tile.
     *
     * @return Image
     */
    public Image getImage() {
        if (tileset != null) {
            return tileset.getImageById(tileImageId);
        } else {
            return internalImage;
        }
    }

    /**
     * Returns a scaled instance of the tile image. Using a MediaTracker
     * instance, this function waits until the scaling operation is done.
     * <p/>
     * Internally it caches the scaled image in order to optimize the common
     * case, where the same scale is requested as the last time.
     *
     * @param zoom the requested zoom level
     * @return Image
     */
    public Image getScaledImage(double zoom) {
        if (zoom == 1.0) {
            return getImage();
        } else if (zoom == myZoom && scaledImage != null) {
            return scaledImage;
        } else {
            Image img = getImage();
            if (img != null)
            {
                scaledImage = img.getScaledInstance(
                        (int)(getWidth() * zoom), (int)(getHeight() * zoom),
                        BufferedImage.SCALE_SMOOTH);

                MediaTracker mediaTracker = new MediaTracker(new Canvas());
                mediaTracker.addImage(scaledImage, 0);
                try {
                    mediaTracker.waitForID(0);
                }
                catch (InterruptedException ie) {
                    System.err.println(ie);
                }
                mediaTracker.removeImage(scaledImage);
                myZoom = zoom;
                return scaledImage;
            }
        }

        return null;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "Tile " + id + " image id " + tileImageId +  "(" + getWidth() + "x" + getHeight() + ")";
    }
    
    public boolean equals (Object o) {
    	if (o instanceof Tile) {
    		Tile t = (Tile)o;
    		
    		if (this.getImageId() == t.getImageId()) {
    			Properties p1 = this.getProperties();
    			Properties p2 = t.getProperties();
    			
    			for (Iterator i = p1.entrySet().iterator(); i.hasNext(); ) {
    				Entry item = (Entry)i.next();
    				if (p2.get(item.getKey()) == null || !p2.get(item.getKey()).equals(item.getValue())) {
    					return false;
    				}
    			}
    			
    			return this.getTransform().equals(t.getTransform());
    		} else {
    			return false;
    		}
    	} else {
    		return false;
    	}
    }
    
    public void rotateLeft () {
    	AffineTransform trans = new AffineTransform();
    	trans.quadrantRotate(-1);
    	transform.preConcatenate(trans);
    }
    
    public void rotateRight () {
    	AffineTransform trans = new AffineTransform();
    	trans.quadrantRotate(1);
    	transform.preConcatenate(trans);
    }
    
    public void flipHorizontal () {
    	AffineTransform trans = new AffineTransform();
    	trans.scale(-1, 1);
    	transform.preConcatenate(trans);
    }
    
    public void flipVertical () {
    	AffineTransform trans = new AffineTransform();
    	trans.scale(1, -1);
    	transform.preConcatenate(trans);
    }
    
    public AffineTransform getTransform () {
    	return this.transform;
    }
    
    public void setTransform (AffineTransform t) {
    	this.transform = t;
    }
}

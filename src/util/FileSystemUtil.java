/*
    This file is part of PTME.

    PTME is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    PTME is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with PTME. If not, see <https://www.gnu.org/licenses/>.

    Copyright (c) 2022  Alex (Pawn Studios)
 */

package util;

import tiled.mapeditor.MapEditor;

import java.io.File;
import java.net.URL;

public class FileSystemUtil {
  public static File getLocalDirectory() {
    try {
      URL url = MapEditor.class.getProtectionDomain().getCodeSource().getLocation();
      File directory = new File(url.toURI()).getParentFile();
      return directory;
    } catch (Exception e) {
      System.out.println("Failed to get local directory.");
      System.out.println(e);
      return new File("");
    }
  }
}

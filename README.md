# Pawn Tactics Map Editor (PTME)

The official Map Editor for Pawn Tactics.   
PTME is built in Java on top of [Tiled Map Editor](https://www.mapeditor.org/).  

Includes a Stamp Builder program as well as changes made by [firstmate](https://pawngame.com/forum/members/firstmate.76/).